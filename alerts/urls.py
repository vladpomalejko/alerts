from django.urls import include, path
from rest_framework import routers

from alerts.views import (
    ClientViewSet,
    MailingsStatView,
    MailingStatView,
    MailingViewSet,
)

router_client = routers.SimpleRouter()
router_mailing = routers.SimpleRouter()

router_client.register(r"clients", ClientViewSet)
router_mailing.register(r"mailings", MailingViewSet)
urlpatterns = [
    path("api/v1/", include(router_client.urls)),
    path("api/v1/", include(router_mailing.urls)),
    path("api/v1/mailing-stats/", MailingsStatView.as_view()),
    path("api/v1/mailing-stats/<int:pk>/", MailingStatView.as_view()),
]
