import re

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

# В условии указано о создании дополнительного поля ID для всех моделей, но мне кажется стандартного ID primary key для этого хватит.


def validate_phone_number(value):
    if not re.match(r"^7\d{10}$", value):
        raise ValidationError("Номер телефона должен быть в формате 7XXXXXXXXXX")


class Client(models.Model):
    tag = models.CharField(max_length=100, blank=True, null=True)
    timezone = models.SmallIntegerField(
        validators=[MinValueValidator(-9), MaxValueValidator(9)]
    )
    code_of_operator = models.SmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(999)]
    )
    phone_number = models.CharField(max_length=11, validators=[validate_phone_number])

    def __str__(self):
        return f"Клиент{self.id}"


class Mailing(models.Model):
    date_of_start = models.DateTimeField()
    date_of_finish = models.DateTimeField()
    client_filter_operator_code = models.SmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(999)], blank=True, null=True
    )
    message = models.ManyToManyField("Message", related_name="mailing_message")

    def __str__(self):
        return f"Рассылка{self.id}"


"""
Сигнал для валидации, чтобы мы в рассылку могли устанавливать только те сообщения, в которых у подписанных клиентов код оператора равен фильтру по коду оператора в рассылке
"""


@receiver(m2m_changed, sender=Mailing.message.through)
def validate_message_for_client_operator(
    sender, instance, action, reverse, model, pk_set, **kwargs
):
    if action == "pre_add":
        for message_id in pk_set:
            message = Message.objects.get(pk=message_id)
            if (
                instance.client_filter_operator_code
                != message.client.first().code_of_operator
            ):
                raise ValidationError(
                    "Выбранное сообщение не соответствует клиенту рассылки."
                )


class Message(models.Model):
    class Status(models.TextChoices):
        DELIVERED = "delivered"
        PENDING = "pending"
        REFUSED = "refused"

    date_of_start = models.DateTimeField()
    status = models.CharField(
        max_length=20, choices=Status.choices, default=Status.PENDING
    )
    client = models.ManyToManyField("Client", related_name="client_message")
    text = models.TextField()

    def __str__(self):
        return f"Сообщение{self.id}"
