from django.db.models import Count
from django.shortcuts import get_object_or_404
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from alerts.models import Client, Mailing, Message
from alerts.serializers import ClientSerializer, MailingSerializer


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()


"""
View для просмотра общей статистики по рассылкам с группировкой по статусам сообщений
"""


class MailingsStatView(GenericAPIView):
    def get(self, request):
        status_counts = (
            Message.objects.values("status")
            .annotate(count=Count("status"))
            .order_by("status")
        )
        status_counts_dict = {item["status"]: item["count"] for item in status_counts}
        data = {
            "total_mailings": Mailing.objects.count(),
            "total_messages": Message.objects.count(),
            "status_counts": status_counts_dict,
        }

        return Response(data)


"""
View для просмотра отдельной рассылки с группировкой по статусам сообщений
"""


class MailingStatView(GenericAPIView):
    lookup_field = "pk"

    def get(self, request, pk):
        mailing = get_object_or_404(Mailing, id=pk)
        status_counts = (
            mailing.message.values("status")
            .annotate(count=Count("status"))
            .order_by("status")
        )
        status_counts_dict = {item["status"]: item["count"] for item in status_counts}

        data = {"total_mailings": 1, "status_counts": status_counts_dict}

        return Response(data)
