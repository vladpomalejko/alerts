from django.contrib import admin

from alerts.models import Client, Mailing, Message

admin.site.register([Client, Mailing, Message])
