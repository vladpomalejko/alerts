import requests
from celery import shared_task
from django.db import transaction
from django.db.models import Q
from django.utils import timezone

from alerts.models import Mailing

headers = {
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjgzODM2ODIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9lbmV2enp5In0.2hZjvo-uuUeJwBqkrLY4OT4o5FoMAXqxEtDeTfVPcu0"
}


@shared_task
@transaction.atomic
def managing_mailings():
    mailings = Mailing.objects.filter(
        Q(date_of_start__lte=timezone.now()) & Q(date_of_finish__gte=timezone.now())
    )
    for mailing in mailings:
        for message in mailing.message.filter(status="pending"):
            for client in message.client.all():
                body = {
                    "id": client.id,
                    "phone": client.phone_number,
                    "text": message.text,
                }
                response = requests.post(
                    url=f"https://probe.fbrq.cloud/v1/send/{client.id}",
                    json=body,
                    headers=headers,
                )
                print(response.status_code)
                if response.status_code == 200:
                    message.status = "delivered"
                    message.save()
                else:
                    print(f"Ошибка запроса. Код состояния: {response.status_code}")
